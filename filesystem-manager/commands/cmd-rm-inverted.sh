#!/bin/bash
#
# Plugin to implement the `rm` command of the config file
# which would remove everything except target(s)
#

#
# Function to remove everything except a specified list in a given directory
#
# @param cd  -- directory to set as the current before do the job
# @param dst -- targets what must stay
#
function cmd_rm_inverted()
{
    local cd="$1"
    local dst="$2"

    if ! verify_dir "${cd}"; then
        eerror "Package image dir is undefined! Skip any actions..."
        return
    fi
    if [ -z "${T:-${TEMP}}" ]; then
        eerror "Package temp dir is undefined! Skip any actions..."
        return
    fi

    local -r image_dir="${D:-${IMAGE}}"
    local -r temp_dir="${T:-${TEMP}}"
    if [ -d "${image_dir}/${cd}" ]; then
        cd "${image_dir}/${cd}"
        eval find ${dst} 2>/dev/null | sed 's,^\./,,' | sort > ${temp_dir}/rm_except.lst
        find | sed 's,^\./,,' | sort | comm -3 ${temp_dir}/rm_except.lst - >${temp_dir}/rm.lst
        if [[ -s ${temp_dir}/rm.lst ]]; then
            rm -vrf $(< ${temp_dir}/rm.lst) 2>/dev/null && schedule_a_warning_after_all
        fi
        cd - >/dev/null
        cleanup_empty_dirs
    fi
}
