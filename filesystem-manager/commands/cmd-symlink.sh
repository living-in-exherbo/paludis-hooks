#!/bin/bash
#
# Plugin to implement the `symlink` command of the config file
#

#
# Function to make a symbolic link
#
# @param cd  -- directory to set as the current before do the job
# @param src -- source name
# @param dst -- destination name
#
function cmd_symlink()
{
    local cd="$1"
    local src="$2"
    local dst="$3"

    if ! verify_dir "${cd}"; then
        eerror "Package image dir is undefined! Skip any actions..."
        return 0
    fi

    local -r image_dir="${D:-${IMAGE}}"
    [[ -d ${image_dir}/$cd && -e ${image_dir}/${cd}/${src} ]] \
      && cd "${image_dir}/${cd}" \
      && ln -vs ${src} ${dst} \
      && schedule_a_warning_after_all \
      && cd - >/dev/null

    return 0
}
