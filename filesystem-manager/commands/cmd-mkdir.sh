#!/bin/bash
#
# Plugin to implement the `mkdir` command of the config file
#

#
# Function to make a directory
#
# @param cd  -- directory to set as the current before making a directory (can be empty)
# @param dst... -- directory name(s) to make
#
function cmd_mkdir()
{
    local cd="$1"
    shift 1

    if ! verify_dir "${cd}"; then
        eerror "Package image dir is undefined! Skip any actions..."
        return 0
    fi

    local -r image_dir="${D:-${IMAGE}}"
    [[ -d "${image_dir}/${cd}" ]] \
      && cd "${image_dir}/${cd}" \
      && mkdir -vp "$@" \
      && schedule_a_warning_after_all \
      && cd - >/dev/null

    return 0
}
