#
# Short aliases for various `cave` (sub)commands
#

alias cc='cave contents'
alias ce='cave executables'
alias cfl='cave fix-linkage'
alias co='cave owner'
alias cr='cave resolve'
alias cu='cave uninstall'
alias cw='cave show'
alias cy='cave sync'

#BEGIN Export some defaults
export CAVE_DEFAULT_SEARCH_INDEX_FILE="${CAVE_DEFAULT_SEARCH_INDEX_FILE:-/var/cache/paludis/cave-search-index}"
export CAVE_DEFAULT_RESUME_FILE="${CAVE_DEFAULT_RESUME_FILE:-/tmp/cave.resume}"
export CAVE_DEFAULT_RESOLVE_ALL_OPTIONS='-c -km -Km -Cs -Si -si -Rn --dump-restarts'
#END Export some defaults

# Update default search index file w/ `cave manage-search-index`
function cm()
{
    cave manage-search-index ${CAVE_DEFAULT_SEARCH_INDEX_FILE:+--create "${CAVE_DEFAULT_SEARCH_INDEX_FILE}"}
}
# Search using default index
function cs()
{
    cave search ${CAVE_DEFAULT_SEARCH_INDEX_FILE:+--index "${CAVE_DEFAULT_SEARCH_INDEX_FILE}"} "$@"
}
# Resolve smth and store default resume file
function crz()
{
    cave resolve ${CAVE_DEFAULT_RESUME_FILE:+--resume-file "${CAVE_DEFAULT_RESUME_FILE}"} "$@"
}
# Resume build from default resume file
function cz()
{
    cave resume -Cs ${CAVE_DEFAULT_RESUME_FILE:+--resume-file "${CAVE_DEFAULT_RESUME_FILE}"} "$@"
}
# Resolve a given set using a bunch of essential options
function _default_complete_resolve_set()
{
    cave resolve \
        ${CAVE_DEFAULT_RESUME_FILE:+--resume-file "${CAVE_DEFAULT_RESUME_FILE}"} \
        ${CAVE_DEFAULT_RESOLVE_ALL_OPTIONS} \
        -P '*/*' \
        -U '*/*' \
        "$@"
}

if [[ ! "${SHELLOPTS}" =~ "posix" ]]; then
    # Update `system` set
    function system-up()
    {
        _default_complete_resolve_set "$@" system
    }
    # Update `world` set
    function world-up()
    {
        _default_complete_resolve_set "$@" world
    }
    # Update `everything` set
    function everything-up()
    {
        _default_complete_resolve_set "$@" installed-packages
    }
fi

#
# Reuse cave bash completer to generate completions for just introduced aliases/functions
#
make_completion_wrapper _cave _cs cave search
complete -o bashdefault -o default -F _cs cs
make_completion_wrapper _cave _cm cave manage-search-index
complete -o bashdefault -o default -F _cm cm
make_completion_wrapper _cave _cc cave contents
complete -o bashdefault -o default -F _cc cc
make_completion_wrapper _cave _cr cave resolve
complete -o bashdefault -o default -F _cr cr
make_completion_wrapper _cave _crz cave resolve
complete -o bashdefault -o default -F _crz crz
make_completion_wrapper _cave _cw cave show
complete -o bashdefault -o default -F _cw cw
make_completion_wrapper _cave _co cave owner
complete -o bashdefault -o default -F _co co
make_completion_wrapper _cave _cu cave uninstall
complete -o bashdefault -o default -F _cu cu
make_completion_wrapper _cave _cy cave sync
complete -o bashdefault -o default -F _cy cy
make_completion_wrapper _cave _cz cave resume
complete -o bashdefault -o default -F _cz cz
