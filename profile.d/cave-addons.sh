#
# A bunch of helper functions for troubleshooting %)
#
# TODO Eliminate `ebuild` mentionings
#

#
# Get full ebuild path for package spec
#
function ebuild-for()
{
    cave print-ebuild-path $* || exit 1
}

#
# Display ebuild contents for package spec
#
function show-ebuild-for()
{
    ${PAGER:-less} $(ebuild-for $*)
}

function _pkg_ebuilds_diff()
{
    local op="$1"
    local pkg="$2"

    [[ -z ${pkg} ]] && return

    local i
    for i in `ebuild-for -i "${pkg}"`; do
        # TODO Adopt for Exherbo
        p=$(ebuild-for "${op}$(basename "${i}" .ebuild)")
        if [[ -n ${p} ]]; then
            diff ${PKG_META_DIFF_OPTIONS:--u} "${i}" "${p}"
        else
            return
        fi
    done
}

#
# Show difference between installed package and the same in a repository
#
# TODO Better name?
#
function pkg-meta-diff()
{
    _pkg_ebuilds_diff '=' $*
}

#
# Show difference between installed ebuild and the next best available version
#
# TODO Better name?
#
function pkg-ebuild-diff()
{
    _pkg_ebuilds_diff '>' $*
}
