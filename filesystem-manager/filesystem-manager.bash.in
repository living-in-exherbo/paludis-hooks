#!/bin/bash
#
# Paludis hook script to do some manipulations right after a package
# installed self into an image directory and before actual installing
# into a system will take place.
#
# Copyright (c), 2010-2022 by Alex Turbov <i.zaufi@gmail.com>
#
# Version: @PROJECT_VERSION@
#

# shellcheck disable=SC1091
source "${PALUDIS_EBUILD_DIR}"/echo_functions.bash

CONFIG_FILE="@CMAKE_INSTALL_FULL_SYSCONFDIR@/paludis/hooks/configs/filesystem-manager.conf"
TEMPLATE_FILE="@CMAKE_INSTALL_FULL_DATAROOTDIR@/paludis-hooks/filesystem-manager/filesystem-manager.xsl"

if [[ ${PALUDIS_FILESYSTEM_HOOK_DO_NOTHING} == 'yes' ]]; then
    einfo 'Filesystem Manager Hook: Do not process any actions due explicit request'
    exit 0
fi

# Output environment to log, if debug enabled
if [[ ${PALUDIS_HOOK_DEBUG} == 'yes' ]]; then
    declare -r _this_hook="${HOOK:-"$(basename "$(dirname "${HOOK_FILE:-${hook_file:-unknown-hook}}")")"}"
    declare -r _subject="${PF:-${PNVR}}"
    declare -r debug_log="/tmp/ph-fsm-${_subject:+${_subject}-}${_this_hook}.${PALUDIS_PID}-$$.env.log"
    env | sort > "${debug_log}"
fi

if [[ ! -f ${CONFIG_FILE} ]]; then
    ewarn 'Filesystem Manager hook: Config file is missing. Nothing to do…'
    exit 0                                                  # Nothing to do w/o config
fi

# Validate config file
if ! xmllint --valid --noout ${CONFIG_FILE}; then
    eerror 'Filesystem Manager hook: Config file is invalid. Ignoring any actions…'
    exit 1                                                  # Nothing to do w/ incorrect config
fi

_fsm_rememberfile="${T:-${TEMP}}/.filesystem_manager_was_here_${PALUDIS_PID}"
export _fsm_rememberfile

function issue_a_warning()
{
    if [[ ${PALUDIS_FILESYSTEM_HOOK_NO_WARNING} != yes ]]; then
        ewarn "WARNING: ${CATEGORY}/${PF:-${PNVR}} package installation was altered by the filesystem manager hook."
        ewarn 'WARNING: Before filing a bug, remove all configured rules, reinstall, and try again…'
    fi
}

function schedule_a_warning_after_all()
{
    [[ ! -e ${_fsm_rememberfile} ]] && touch "${_fsm_rememberfile}"
    return
}
export -f schedule_a_warning_after_all

function verify_dir()
{
    local -r _vd_image_dir="${D:-${IMAGE}}"
    if [[ -n ${_vd_image_dir} ]]; then
        # Make sure we have smth real in `D`/`IMAGE`!
        local -r _vd_p1=$(realpath -mq "${1}")
        local -r _vd_p2=$(realpath -mq "${_vd_image_dir}/$1")
        [[ ${_vd_p1} != "${_vd_p2}" ]]
        return
    fi
    return 1
}
export -f verify_dir

function cleanup_empty_dirs()
{
    local -r _ced_image_dir="${D:-${IMAGE}}"
    # Walk through whole image and try to remove possible empty dirs
    # ATTENTION According EAPI it is incorrect to install empty directories!
    # If a package need some, then its ebuild must use `keepdir` for this!
    # So this action also can be considered as sanitize an image before install :)
    find "${_ced_image_dir}" -type d -a -empty -exec rmdir -p --ignore-fail-on-non-empty {} +
    # Sometimes (if u really don't want a WHOLE package, but have to install it,
    # like boring kde-wallpapers) the last command may delete even `${image_dir}` directory,
    # so paludis will complain about broken image :) -- Ok, lets restore it!
    [[ ! -e ${_ced_image_dir} ]] && mkdir -p "${_ced_image_dir}"
}
export -f cleanup_empty_dirs

result=0
case "${HOOK}" in
    # ATTENTION This script must be symlinked to the following hook dirs:
    ebuild_install_post)
        tmp_file="$(mktemp "${T:-${TEMP}}"/fsmh.XXXXXXX.sh)"
        xsltproc -o "${tmp_file}" \
            --stringparam 'PN' "${PN}" \
            --stringparam 'PF' "${PF:-${PNVR}}" \
            --stringparam 'PR' "${PR}" \
            --stringparam 'PV' "${PV}" \
            --stringparam 'PVR' "${PVR}" \
            --stringparam 'CATEGORY' "${CATEGORY}" \
            --stringparam 'REPOSITORY' "${REPOSITORY}" \
            --stringparam 'SLOT' "${SLOT}" \
            --stringparam 'debug' "${PALUDIS_HOOK_DEBUG}" \
            "${TEMPLATE_FILE}" "${CONFIG_FILE}"

        if [[ ${PALUDIS_HOOK_DEBUG} == 'yes' ]]; then
            einfo 'Script to execute:'
            cat "${tmp_file}"
        fi

        bash "${tmp_file}"
        result=$?

        [[ -e ${_fsm_rememberfile} ]] && issue_a_warning
        ;;
esac

exit ${result}

# kate: hl bash;
